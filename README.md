# Important jupyter notebooks (for Colab)

* [Yolo (Combined 2 Models)](https://colab.research.google.com/drive/1blHbZIbkuHouwTtuRQq3PotqJYNhfyFy)
* [Yolo (Custom Training)](https://colab.research.google.com/drive/1amRUdPMpyVk0Rz11S44Px203yk0RiKgy)


# Important files
*   [Yolo Weights](https://drive.google.com/open?id=12hk90ds9aa3zf3Cn62DdJiI3Btk55cDk) (**Put this into model_data folder**)
*   [Weight file trained on custom classes (houses and flowers)](https://drive.google.com/file/d/1uiccHFQ3D6RC0E2RktE380d4sfBcvh2w/view)


# Run for MOT Challenge
```
python evaluate_motchallenge_using_yolo.py --mot_dir MOT16/train/ --detection_dir resources/detections/MOT16_POI_train/ --min_confidence=0.3 --nn_budget=100 --output_dir=output/using_yolo_train
```

# Evaluating result 
```
python eval_MOT.py MOT16/train/ output/using_yolo_train/
```

# Run for custom video
```
python demo.py [video file]
```
The output in MOT format is saved into output.txt file